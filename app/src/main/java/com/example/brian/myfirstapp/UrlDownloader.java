package com.example.brian.myfirstapp;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class UrlDownloader extends AsyncTask<String, Integer, String> {

    protected String doInBackground(String... urls) {
        String theUrlWeReallyWant = urls[0];
        try {
            URLConnection connection = new URL(theUrlWeReallyWant).openConnection();
            connection.setReadTimeout(10*1000);
            connection.connect();
            InputStreamReader streamReader = new InputStreamReader(connection.getInputStream());

            BufferedReader reader = new BufferedReader(streamReader);
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line); stringBuilder.append("\n");
            }
            Log.d("UrlDownloader", stringBuilder.toString());
            return stringBuilder.toString();
        } catch (IOException e) {
            Log.d("UrlDownloader", e.getMessage());
            e.printStackTrace();
        }
        return "";
    }
}
