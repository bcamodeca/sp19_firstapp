package com.example.brian.myfirstapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.*;


import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class MainActivity extends AppCompatActivity {

    private TextView tvOutput;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private String zipCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Brian was here!
        // The spring 2019 run of Android is the best class ever in the history of education.
        preferences = getPreferences(Context.MODE_PRIVATE);
        editor = preferences.edit();

        String defaultZipCode = preferences.getString(getString(R.string.prefLastZipCode), "");

        tvOutput = findViewById(R.id.tvOutput);
        final EditText txtZip = findViewById(R.id.txtZip);
        final Button btnBest = findViewById(R.id.best_button);

        txtZip.setText(defaultZipCode);

        btnBest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zipCode = txtZip.getText().toString().trim();

                if (zipCode.matches(getString(R.string.validZipCodeRegex))) {
                    editor.putString(getString(R.string.prefLastZipCode), zipCode);
                    editor.commit();
                    if (preferences.contains("zipResult_" + zipCode)) {
                        String cachedPair = preferences.getString("zipResult_" + zipCode, "");
                        String[] cachedPairSplit = cachedPair.split(",");
                        String latitude = cachedPairSplit[0];
                        String longitude = cachedPairSplit[1];
                        String pair = String.format("Lat: %s\u00b0, Lng: %s\u00b0", latitude, longitude);
                        tvOutput.setText(pair);

                    } else {
                        String geoNamesApiBase = "https://secure.geonames.org/postalCodeSearchJSON?username=camodeca002&maxRows=1&postalcode=";
                        String urlWeWantToCall = geoNamesApiBase + zipCode;
                        Ion.with(getApplicationContext())
                                .load(urlWeWantToCall)
                                .asJsonObject()
                                .setCallback(new FutureCallback<JsonObject>() {
                                    @Override
                                    public void onCompleted(Exception e, JsonObject result) {
                                        tvOutput.setText(result.toString());
                                    }
                                });
                        /*
                        String geoNamesApiBase = "https://secure.geonames.org/postalCodeSearchJSON?username=camodeca002&maxRows=1&postalcode=";
                        String urlWeWantToCall = geoNamesApiBase + zipCode;
                        CoordinateGetter downloader = new CoordinateGetter();
                        downloader.execute(urlWeWantToCall);
                        */
                    }

                    //Toast.makeText(getApplicationContext(), )
                    Toast loadingToast = Toast.makeText(MainActivity.this, "Loading...", Toast.LENGTH_LONG);
                    //loadingToast.setGravity(Gravity.TOP | Gravity.LEFT,0,0 );
                    loadingToast.setGravity(Gravity.CENTER, 0, 0);
                    loadingToast.show();
                } else {
                    txtZip.setError(getString(R.string.invalidZipCodeError));
                }


            }
        });
    }

    private class CoordinateGetter extends UrlDownloader {
        @Override
        protected void onPostExecute(String result) {
            Log.d("CoordinateGetter", "got this far!");
            try {
                JSONObject parsedJson = new JSONObject(result);
                JSONArray coordinateResults = parsedJson.getJSONArray("postalCodes");
                JSONObject firstResult = coordinateResults.getJSONObject(0);
                String latitude = firstResult.getString("lat");
                String longitude = firstResult.getString("lng");
                String pair = String.format("Lat: %s\u00b0, Lng: %s\u00b0", latitude, longitude);

                editor.putString("zipResult_" + zipCode, latitude + "," + longitude);
                editor.commit();

                Log.wtf("Coordinate Getter", "parsing result");
                tvOutput.setText(pair);
                //tvOutput.setText(latitude + ", " + longitude);
            } catch (JSONException e) {
                Log.d("CoordinateGetter", "Invalid JSON");
            }

        }
    }

}
